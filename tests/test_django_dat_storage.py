import pytest
from unittest import mock
from django.core.exceptions import ImproperlyConfigured
from django.core.files.base import ContentFile
from django.test import TestCase, override_settings

from django_dat_storage import DatStorage


class DatStorageTestCase(TestCase):
    @mock.patch('django_dat_storage.DatStorage')
    def setUp(self, mDatStorage):
        self.mocked_storage = mDatStorage()
        self.storage = DatStorage()


class DatStorageTests(DatStorageTestCase):

    def test_url_dat_without_gateway(self):
        assert 'dat://' in self.storage.url('788081e2c277644d15713f5e54b97d046672ad1eb637660e6c80d17c29db7507')

    def test_url_http_without_gateway(self):
        assert 'dat://' in self.storage.url('788081e2c277644d15713f5e54b97d046672ad1eb637660e6c80d17c29db7507', http=True)

    @override_settings(DAT_STORAGE_GATEWAY_URL='http://localhost:3000')
    def test_url_dat_with_gateway(self):
        storage = DatStorage()
        assert 'dat://' in storage.url('788081e2c277644d15713f5e54b97d046672ad1eb637660e6c80d17c29db7507', http=False)

    @override_settings(DAT_STORAGE_GATEWAY_URL='http://localhost:3000')
    def test_url_http_with_gateway(self):
        storage = DatStorage()
        assert 'http://localhost:3000/{}'.format('788081e2c277644d15713f5e54b97d046672ad1eb637660e6c80d17c29db7507') == storage.url('788081e2c277644d15713f5e54b97d046672ad1eb637660e6c80d17c29db7507')

    def test_path_without_storage_root(self):
        self.assertRaises(ImproperlyConfigured, self.storage.path, '788081e2c277644d15713f5e54b97d046672ad1eb637660e6c80d17c29db7507')

    @override_settings(DAT_STORAGE_API_ROOT='./')
    def test_path_with_storage_root(self):
        storage = DatStorage()
        assert '.yarnrc' == storage.path('788081e2c277644d15713f5e54b97d046672ad1eb637660e6c80d17c29db7507').__str__()

    def test_size(self):
        assert self.storage.size('788081e2c277644d15713f5e54b97d046672ad1eb637660e6c80d17c29db7507') == 0
