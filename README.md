django-dat-storage
===================

Store [Django file-uploads](https://docs.djangoproject.com/en/2.1/topics/http/file-uploads/)
on [Dat](https://datproject.org/).

Uploads are added to the configured [dat-daemon](https://github.com/soyuka/dat-daemon) node,
which returns the Dat Key. This hash is the name that is saved to your database. Modifications
to the file are not supported yet, but Dat allows it so this is a future improvement of the package.

Other Dat users access and reseed a piece of content 
through its unique key. Differently-distributed (i.e. normal HTTP) users
can access the uploads through an [HTTP→Dat gateway](https://github.com/soyuka/dat-daemon/tree/master/packages/http) that is optional.


Installation
------------

```bash
pip install django-dat-storage
```

Configuration
-------------

By default `django-dat-storage` adds content to a [dat-daemon](https://github.com/soyuka/dat-daemon) running on localhost
and optionally returns URLs pointing to the public HTTP Gateway (otherwise plain Dat uris).

To customise this, set the following variables in your `settings.py`:

- `DAT_STORAGE_API_URL`: defaults to `'ws://localhost:8447'`. 
- `DAT_STORAGE_GATEWAY_URL`: defaults to `'https://Dat.io/Dat/'`.
  
Set `DAT_STORAGE_GATEWAY_URL` to `'http://localhost:3000/'` to serve content
through your local daemon's HTTP gateway.


Usage
-----

There are two ways to use a Django storage backend.

### As default backend

Use Dat as [Django's default file storage backend](https://docs.djangoproject.com/en/1.11/ref/settings/#std:setting-DEFAULT_FILE_STORAGE):

```python
# settings.py

DEFAULT_FILE_STORAGE = 'django-dat-storage.DatStorage'

DAT_STORAGE_API_URL = 'ws://localhost:8447/'
DAT_STORAGE_GATEWAY_URL = 'http://localhost:3000/'
```  


### For a specific FileField

Alternatively, you may only want to use the Dat storage backend for a single field:

```python
from django.db import models

from django_dat_storage import DatStorage 


class MyModel(models.Model):
    # …
    file_stored_on_Dat = models.FileField(storage=DatStorage()) 
    other_file = models.FileField()  # will still use DEFAULT_FILE_STORAGE
```

Don't forget the brackets to instantiate `DatStorage()` with the default arguments!


FAQ
---

### Why Dat?

See <https://docs.datproject.org/#why-dat>. 

### How do I delete an upload?

Because of the distributed nature of Dat, anyone who accesses a piece
of content keeps a copy, and reseeds it for you automatically until it's 
evicted from their node's local cache.

Unfortunately, if you're trying to censor yourself, this means the best 
we can do is remove the piece of content from your own Dat node(s)
and hope nobody else has archived it before on their own node.
