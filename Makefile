.PHONY: default
default: build;

clean:
	rm -rf ./dist ./*.egg-info ./pip-wheel-metadata/

build: clean
	@command -v poetry >/dev/null 2>&1 || curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python # if we lack poetry, we can install it
	@poetry build

test:
	@command -v poetry >/dev/null 2>&1 || curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python # if we lack poetry, we can install it
	@poetry run pytest

