from urllib.parse import urlparse
from pathlib import Path

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.core.files.base import File, ContentFile
from django.core.files.storage import Storage
from django.utils.deconstruct import deconstructible

from dat_daemon_pyclient import DatDaemonClient


@deconstructible
class DatStorage(Storage):
    """Dat Django storage backend.

    Only file creation and reading is supported for now, but Dat supports modification in place.
    """
    def __init__(self, daemon_url=None, gateway_url=None, storage_root=None):
        """Connect to a dat-daemon to add/remove files.

        :param daemon_url: Dat control API base URL.
                        Also configurable via `settings.DAT_STORAGE_API_URL`.
                        Defaults to 'ws://localhost:8477'.
        :param gateway_url: Optional. Dat HTTP gateway to the daemon.
                            Also configurable via `settings.DAT_STORAGE_GATEWAY_URL`.
                            Defautls to None.
        """
        self._dat_client = DatDaemonClient(
            url=daemon_url or getattr(settings, 'DAT_STORAGE_API_URL', 'ws://localhost:8477')
        )
        self._dat_gateway_url = gateway_url or getattr(settings, 'DAT_STORAGE_GATEWAY_URL', None)
        self._dat_storage_root = storage_root or getattr(settings, 'DAT_STORAGE_API_ROOT', None)  # None if the daemon is remote

    def open(self, name: str, mode='rb') -> File:
        """Retrieve the file content identified by the key.

        :param name: Dat Key.
        :param mode: Ignored. The returned File instance is read-only.
        """
        return ContentFile(self._dat_client.create_read_stream(name, path=''), name=name)

    def save(self, name: str, content: File, **kwargs) -> str:
        """Add content to the Dat daemon.

        :param name: Ignored. Provided to comply with `Storage` interface.
        :param content: Django File instance to save.
        :return: Dat Key.
        """
        with open(Path(self._dat_storage_root, name), 'wb') as t:
            for _bytes in content.__iter__():
                t.write(_bytes)
        key = self._dat_client.add(path=name)['key']
        return key

    def get_valid_name(self, name):
        """Returns name. Only provided for compatibility with Storage interface."""
        return name

    def get_available_name(self, name, max_length=None):
        """Returns name. Only provided for compatibility with Storage interface."""
        return name

    def size(self, name: str) -> int:
        """Total size, in bytes, of Dat with key `name`."""
        return self._dat_client.info(name)['statistics']['byteLength']

    def delete(self, name: str):
        """Delete Dat content from the daemon."""
        self._dat_client.remove(name)

    def url(self, name: str, http=True):
        """Returns an HTTP-accessible Gateway URL by default.

        Override this if you want direct `Dat://…` URLs or something.

        :param name: Dat Key.
        :param http: should we display the HTTP Gateway url instead? Defaults to True.
        :return: HTTP URL to access the content via an Dat HTTP Gateway.
        """
        if self._dat_gateway_url and http:
            return '{url}/{key}'.format(url=self._dat_gateway_url, key=name)
        return 'dat://{}'.format(name)

    def path(self, name):
        if not self._dat_storage_root:
            raise ImproperlyConfigured('Daemon storage root was not properly configured '
                                       'and is required to return a local path. Please '
                                       'set DAT_STORAGE_API_ROOT to the local root directory '
                                       'where dat-daemon is run.'
                                       )

        ret = self._dat_client.readdir(name)
        if self.exists(name):
            return Path(self._dat_storage_root, ret['files'][0])
        return False

    def exists(self, name):
        return 'statistics' in self._dat_client.info(name).keys()

    def listdir(self, path):
        raise NotImplementedError

    def get_accessed_time(self, name):
        raise NotImplementedError

    def get_created_time(self, name):
        raise NotImplementedError

    def get_modified_time(self, name):
        raise NotImplementedError
